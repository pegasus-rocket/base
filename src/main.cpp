#include "settings.h"
#include <Arduino.h>
#include "sound.h"
#include "main.h"
#include <SPI.h>
#include "radio.h"

RocketAction action = {.fireParachute = false, .resetStorage = false};
Base base = Base_new();
Radio radio;

constexpr int launcherBtn = A0;
constexpr int emergencyBtn = A1;

unsigned long rocketConnectedTimeMemory = 0; // Set false by default
unsigned long telemetryTimeMemory = 0;

constexpr int ledIgnitedPin = 3;
constexpr int ledReadyPin = 4;
constexpr int ledPowerPin = 5;

unsigned long rocketActionTimeMemory = millis();

void setup() {
    Serial.begin(SERIAL_BAUD_RATE);
    Serial.setTimeout(20);
    Sl.setLogLevel(DEBUG);
    Sl.enableLevelName(true);

    pinMode(launcherBtn, INPUT);
    pinMode(emergencyBtn, INPUT);

    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(ledPowerPin, OUTPUT);
    pinMode(ledReadyPin, OUTPUT);
    pinMode(ledIgnitedPin, OUTPUT);

    digitalWrite(ledPowerPin, HIGH);
    digitalWrite(ledReadyPin, HIGH);
    digitalWrite(ledIgnitedPin, HIGH);
    playTone(800, 1000);

    delay(1000);

    digitalWrite(ledReadyPin, LOW);
    digitalWrite(ledIgnitedPin, LOW);

    radio.begin();
    radio.addEventListener<Rocket>(Radio::ROCKET_DATA, onRocketData);


    // Block boot to avoid rocket ignition by error.
    if (analogRead(emergencyBtn) <= 1 || analogRead(launcherBtn) > 2000) {
        base.locked = true;
    }

    delay(100);
}

void loop() {
    radio.update();
    readSerial();
    if (millis() - rocketConnectedTimeMemory > 1000)
        base.isRocketConnected = false;

    if (millis() - rocketActionTimeMemory > 100) {
        rocketActionTimeMemory = millis();
        if (action.fireParachute && base.rocket.devices.parachute.status != FIRED)
            releaseParachute();
        if (base.motor.status == FIRED)
            releaseRocket();
    }


    if (analogRead(emergencyBtn) <= 1) {
        // Parachute btn is pushed !
    }


    if (analogRead(launcherBtn) > 2000) {
        releaseRocket();
    }

    if (millis() - telemetryTimeMemory > TELEMETRY_RATE) {
        // Serial.println(base.toJSON());
        Serial.println(base.toJSON().c_str());
        telemetryTimeMemory = millis();
    }
}

void updateStatus() {
    if (
        base.isRocketConnected
        && base.rocket.devices.system.status == READY &&
        !base.locked) {
        digitalWrite(ledReadyPin, HIGH);
    }
}

void readSerial() {
    if (Serial && Serial.available() > 0) {
        String str = Serial.readString(); // read until timeout
        str.trim();

        Serial.println(str);
        if (str == "release_rocket")
            base.motor.status = FIRED;
        else if (str == "release_parachute")
            action.fireParachute = true;
        else if (str == "clear_rocket_storage")
            action.resetStorage = true;
    }
}

void releaseRocket() {
    if (!base.isRocketConnected) return;
    if (base.rocket.devices.system.status == READY && !base.locked) {
    }
}

void releaseParachute() {
    sendRocketAction();
}

void resetStorage() {
    sendRocketAction();
}

void sendRocketAction() {
}

void onRocketData(const Rocket *rocket) {
    memcpy(&base.rocket, rocket, sizeof(Rocket));
    base.isRocketConnected = true;
    rocketConnectedTimeMemory = millis();
}
