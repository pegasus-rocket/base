#include "logger.h"

logger::Logger Sl;

void logger::Logger::log(const LogLevel level, const char *format, const va_list args) const {
    if (Serial && level != HIDDEN && level >= logLevel) {
        char buffer[1024];

        const int len = vsnprintf(buffer, sizeof(buffer), format, args);

        if (len >= sizeof(buffer)) {
            buffer[sizeof(buffer) - 1] = '\0';
        }
        if (logLevelEnabled)
            Serial.printf("[%s] %s", logLevelToString(level), buffer);
        else
            Serial.printf("%s", buffer);
    }
}

void logger::Logger::log(const LogLevel level, const char *format, ...) const {
    va_list args;
    va_start(args, format);
    log(level, format, args);
    va_end(args);
}

void logger::Logger::debug(const char *format, ...) const {
    va_list args;
    va_start(args, format);
    log(DEBUG, format, args);
    va_end(args);
}

void logger::Logger::info(const char *format, ...) const {
    va_list args;
    va_start(args, format);
    log(INFO, format, args);
    va_end(args);
}

void logger::Logger::warning(const char *format, ...) const {
    va_list args;
    va_start(args, format);
    log(WARN, format, args);
    va_end(args);
}

void logger::Logger::error(const char *format, ...) const {
    va_list args;
    va_start(args, format);
    log(ERROR, format, args);
    va_end(args);
}

const char *logger::Logger::logLevelToString(const LogLevel level) {
    switch (level) {
        case DEBUG: return "DEBUG";
        case INFO: return "INFO";
        case WARN: return "WARNING";
        case ERROR: return "ERROR";
        default: return "UNKNOWN";
    }
}
