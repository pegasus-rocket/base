#pragma once
#include "Arduino.h"
#include <atomic>
#include "logger.h"
#include "vector.h"

using namespace std;

// This file contains all the base's settings.

// --- GENERAL SETTINGS ---

// Serial baud rate
#define SERIAL_BAUD_RATE 500000

// --- RADIO ---
// The section below is about radio's settings.

// Time interval in milliseconds to send the rocket's telemetry.
#define TELEMETRY_RATE 30

#define RADIO_MAX_PAYLOAD_SIZE 80

#define RADIO_PIN_CS 10
#define RADIO_PIN_IRQ 7
// #define RADIO_PIN_IRQ 4
#define RADIO_PIN_RST 8
#define RADIO_PIN_GPIO 9

#define RADIO_FREQ 868.3
#define RADIO_BR 15.0
#define RADIO_FREQ_DEV 57.136417
#define RADIO_RXBW 234.3
#define RADIO_POWER 10
#define RADIO_PREAMBLE_LENGTH 32

// Radio rocket address
#define RADIO_ROCKET_ADDRESS 01

// Base rocket address
#define RADIO_BASE_ADDRESS 00
#define RADIO_CHANNEL 90
