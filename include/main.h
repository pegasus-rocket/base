#include <stdint.h>
#include "Arduino.h"
#include "json.h"
#include "Rocket.h"

typedef struct RocketAction_s {
    bool fireParachute;
    bool resetStorage;
} __attribute__((packed, aligned(1))) RocketAction;

typedef struct Base_s {
    Rocket rocket;
    Device motor;
    bool isRocketConnected;
    bool locked;

    [[nodiscard]] std::string toJSON(bool isKey = false) const {
        std::ostringstream json;

        if (!isKey) json << "{";

        // Serialize orientation to JSON
        json << R"("device_type": "base",)";
        json << serializeToJson(isRocketConnected, "isRocketConnected") << ",";
        json << serializeToJson(rocket.toJSON(true), "rocket", CURLY_BRACKET) << ",";
        json << serializeToJson(motor.toJSON(true), "motor", CURLY_BRACKET) << ",";
        json << serializeToJson(locked, "locked");

        if (!isKey) json << "}";

        return json.str();
    }
} Base;

inline Base Base_new() {
    Base base;
    base.rocket = Rocket_new();
    base.motor.status = READY;
    base.locked = false;
    base.isRocketConnected = false;
    return base;
}

void readSerial();

void releaseRocket();

void onRocketData(const Rocket *rocket);

void releaseParachute();

void resetStorage();

void sendRocketAction();
