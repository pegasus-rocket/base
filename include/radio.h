#pragma once
#include "settings.h"
#include <map>
#include <list>
#include <memory>
#include <deque>

#include "./../lib/RadioLib/src/RadioLib.h"

using namespace std;

#if defined(ESP8266) || defined(ESP32)
ICACHE_RAM_ATTR
#endif

template<typename T>
using RCallback = void (*)(const T *message);

class Radio {
public:
    Radio(): sx1262(new Module(RADIO_PIN_CS,RADIO_PIN_IRQ,RADIO_PIN_RST,RADIO_PIN_GPIO)) {
    }

    enum __attribute__((packed)) RadioMessageType {
        ROCKET_DATA = 65,
        ROCKET_ACTION = 66
    };

    typedef struct {
        u8_t destinationAddress;
        const void *message;
        uint16_t len;
        RadioMessageType messageType;
    } RadioMessage;

    /**
     * Init the radio module
     * @return Radio status https://jgromes.github.io/RadioLib/group__status__codes.html
     */
    int16_t begin();

    /**
     * Check for a new message. Must be called as often as possible
     */
    void update();

    /**
     * Add event listener for a new message and call a callback function.
     * @tparam T Any type
     * @param type The message type to listen.
     * @param c_handler The function to call as callback when a message is received. Parameter should be the same type as T.
     */
    template<typename T>
    void addEventListener(RadioMessageType type, RCallback<T> c_handler) {
        eventListenerStore[type].emplace_back(
            [c_handler](const void *message) {
                c_handler(static_cast<const T *>(message));
            }
        );
    }

    /**
     * Send a message over the radio.
     * @param message The message to send.
     */
    void sendMessage(RadioMessage *message);

private:
    // Store the callback function
    std::map<RadioMessageType, std::list<std::function<void(const void *)> > > eventListenerStore;
    // Store the message in queue and send them when possible.
    deque<unique_ptr<RadioMessage> > messageQueue;
    SX1262 sx1262;

    int transmissionState = RADIOLIB_ERR_NONE;

    bool transmitFlag = false; // When true, the radio is in transmitting mod.
    volatile static bool operationDone; // (interrupt) True when send or receive action are done.
    volatile static bool isRxReady; // True when receiving action is ready.

    // Radio header
    typedef struct {
        RadioMessageType type;
        uint8_t length;
        uint8_t data[];
    } __attribute__((packed, aligned(1))) RadioHeader;

    static void setOperationDone() {
        // we sent or received a packet, set the flag
        operationDone = true;
    }
};
